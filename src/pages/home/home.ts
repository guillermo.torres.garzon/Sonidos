import { Component } from '@angular/core';
import { ANIMALES } from "../../data/data.animales";
import { Animal } from "../../interfaces/animal.interface";
import { Refresher, reorderArray } from "ionic-angular";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  animales:Animal[] = [];
  audio = new Audio
  idTimer:any;
  blnSeEstaOrdenando:boolean=false;

  constructor() {
    //slice crea un clon del objeto, no la referencia
    this.animales = ANIMALES.slice(0);
  }

  reproducir(animal:Animal){
    this.pausar_audio(animal);

    if(animal.reproduciendo)
    {
      animal.reproduciendo = false;
      return;
    }

    console.log(animal);

    this.audio.src = animal.audio;
    this.audio.load();
    this.audio.play();
    animal.reproduciendo = true;
    this.idTimer = setTimeout(()=>animal.reproduciendo = false, animal.duracion * 1000);
  }

  private pausar_audio(animalSeleccionado:Animal)
  {
    clearTimeout(this.idTimer);
    this.audio.pause();
    this.audio.currentTime = 0;

    for(let animal of this.animales)
    {
      if (animal.nombre != animalSeleccionado.nombre)
      {
        animal.reproduciendo = false;
      }
    }
  }

  borrar_animal(indice:number)
  {
    this.animales.splice(indice,1);
  }

  doRefresh(refresher:Refresher)
  {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      //slice crea un clon del objeto, no la referencia
      this.animales = ANIMALES.slice(0);
      refresher.complete();
    }, 2000);
  }

  ReordenarArreglo(indices:any){
    console.log(indices);
    this.animales = reorderArray(this.animales,indices);
  }

}
